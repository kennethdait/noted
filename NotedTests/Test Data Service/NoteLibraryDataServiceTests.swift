//
//  NoteLibraryDataServiceTests.swift
//  NotedTests
//
//  Created by kend on 11/5/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import Noted

class NoteLibraryDataServiceTests: XCTestCase {

    var sut: NoteLibraryDataService!
    var noteManager: NoteManager!
    var tableView: UITableView!
    
    let testNote1 = Note(content: "test note 1")
    let testNote2 = Note(content: "test note 2")
    let testNote3 = Note(content: "test note 3")
    
    override func setUp()
    {
        super.setUp()
        noteManager = NoteManager()
        sut = NoteLibraryDataService()
        sut.noteManager = noteManager
        tableView = UITableView()
        tableView.dataSource = sut
        tableView.delegate = sut
    }
    
    override func tearDown()
    { super.setUp() }
    
    // MARK: Initialization
    
    func testInit_NoteLibraryDataServicee_Exists()
    { XCTAssertNotNil(sut) }
    
    // MARK: BASIC TABLE CONFIG
    
    func testConfig_NumberOfSections_ReturnsOne()
    { XCTAssertEqual(sut.numberOfSections(in: tableView), 1)}
    
    func testConfig_NumberOfRows_ReturnsNoteCountFromNoteManager()
    {
        noteManager.addNote(testNote1)
        XCTAssertEqual(noteManager.noteCount, 1)
        XCTAssertEqual(sut.tableView(tableView, numberOfRowsInSection: 0), 1)
        
        noteManager.addNote(testNote2)
        XCTAssertEqual(noteManager.noteCount, 2)
        XCTAssertEqual(sut.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    // MARK: ADD/DELETE ROWS

}
