//
//  NoteStructTests.swift
//  NotedTests
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import Noted

class NoteStructTests: XCTestCase
{
    
    let testContent = "blah blah"
    let testDate1 = Date().addingTimeInterval(-2000000)
    let testDate2 = Date().addingTimeInterval(-2000000)
    
    
    override func setUp()
    {
        super.setUp()
        
    }

    // MARK: Initialization
    
    func testInit_NoteWithContent_Exists()
    {
        let testNote = Note(content: testContent)
        XCTAssertNotNil(testNote)
        XCTAssertEqual(testNote.content, testContent)
    }
    
    // MARK: Date Metadata
    
    func testInit_NoteWithCreationDate_SetsProgramatically()
    {
        let testNote = Note(content: testContent)
        print(testNote.whenCreated())
        XCTAssertNotNil(testNote.whenCreated())
    }
    
    func testInit_NoteWithLastModifiedDate_InitializesAsCreationDate()
    {
        let testNote = Note(content: "test note", dateCreated: testDate1)
        let modifiedDate = testNote.whenLastModified()
        XCTAssertEqual(testNote.whenCreated(), modifiedDate)
    }
    
    func testDates_UpdatingNote_ChangesLastModDate_DateCreatedIsImmutable()
    {
        var testNote = Note(content: "test note", dateCreated: testDate1)
        testNote.content = "updated contents"
        XCTAssertNotEqual(testNote.whenCreated(), testNote.whenLastModified())
    }
    
    // MARK: Equatable
    
    func testEquatable_SameContentSameCreationDate_ReturnsNotEqual()
    {
        let testDate = Date()
        let note1 = Note(content: testContent, dateCreated: testDate)
        let note2 = Note(content: testContent, dateCreated: testDate)
        XCTAssertEqual(note1, note2)
    }
    
    func testEquatable_SameContentDifferentCreationDate_ReturnsNotEqual()
    {
        let testDate1 = Date().addingTimeInterval(-30000)
        let testDate2 = Date()
        let note1 = Note(content: testContent, dateCreated: testDate1)
        let note2 = Note(content: testContent, dateCreated: testDate2)
        XCTAssertNotEqual(note1, note2)
    }
    
}
