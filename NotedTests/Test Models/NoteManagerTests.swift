//
//  NoteManagerTests.swift
//  NotedTests
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import Noted

class NoteManagerTests: XCTestCase {

    var sut: NoteManager!
    
    let note1 = Note(content: "Note 1", dateCreated: Date().addingTimeInterval(-30000))
    let note2 = Note(content: "Note 2")
    let note3 = Note(content: "Note 3", dateCreated: Date().addingTimeInterval(30000))
    
    override func setUp()
    {
        sut = NoteManager()
    }

    override func tearDown() { super.tearDown() }

    // MARK: INIT
    
    func testInit_NoteManager_Exists()
    { XCTAssertNotNil(sut)}
    
    func testInit_NoteCount_InitializesToZero()
    { XCTAssertEqual(sut.noteCount, 0)}
    
    // MARK: ADD & QUERY
    
    func testAdd_AddingNote_IncrementsNoteCount()
    {
        sut.addNote(note1)
        XCTAssertEqual(sut.noteCount, 1)
        sut.addNote(note2)
        XCTAssertEqual(sut.noteCount, 2)
    }

    func testQuery_ReturnsNoteAtIndex()
    {
        sut.addNote(note3)
        let noteQueried = sut.noteAt(index: 0)
        XCTAssertEqual(noteQueried, note3)
    }
    
    // MARK: MODIFY & DELETE
    
    func testModify_UpdatesNoteAtIndex()
    {
        sut.addNote(note1)
        try! sut.updateNoteAt(index: 0, with: note3)
        let noteAtIndexZero = sut.noteAt(index: 0)
        XCTAssertEqual(noteAtIndexZero, note3)
    }
    
    func testDelete_DecrementsNoteCount()
    {
        sut.addNote(note2)
        XCTAssertEqual(sut.noteCount, 1)
        _ = sut.deleteNoteAt(index: 0)
        XCTAssertEqual(sut.noteCount, 0)
    }
    
    func testDelete_ReturnsDeletedNote()
    {
        sut.addNote(note1)
        let deletedNote = sut.deleteNoteAt(index: 0)
        XCTAssertNotNil(deletedNote)
        XCTAssertEqual(deletedNote, note1)
    }
    
    func testDelete_ClearArrays_ReturnsCountOfZero()
    {
        sut.addNote(note1)
        sut.addNote(note2)
        XCTAssertEqual(sut.noteCount, 2)
        sut.clearAllNotes()
        XCTAssertEqual(sut.noteCount, 0)
    }
    
}
