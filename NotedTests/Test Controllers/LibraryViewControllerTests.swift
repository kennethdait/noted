//
//  LibraryViewControllerTests.swift
//  NotedTests
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import Noted

class LibraryViewControllerTests: XCTestCase {

    var sut: LibraryViewController!
    var libraryTableView: UITableView!
    
    override func setUp()
    {
        sut = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "libraryVC") as! LibraryViewController)
        _ = sut.view
        libraryTableView = sut.libraryTableView
    }
    
    override func tearDown()
    { super.tearDown() }
    
    // MARK: Init
    
    func testInit_LibraryVC_Exists()
    { XCTAssertNotNil(sut) }
    
    func testInit_LibraryVC_InstantiatesTableView()
    { XCTAssertNotNil(libraryTableView)}
    
    //  MARK: SETS NAVCONTROLLER PROPERTIES
    
    func testInit_NavController_PropertiesSet()
    {
        XCTAssertNotNil(sut.navigationItem)
        XCTAssertEqual(sut.navigationItem.title, sut.appTitle)
        XCTAssertEqual(sut.navigationItem.rightBarButtonItem, sut.newNoteButton)
        XCTAssertEqual(sut.navigationItem.leftBarButtonItem, sut.editButtonItem)
    }
    
    // MARK: TableView Data Source
    
    func testDataSource_ViewDidLoad_SetsTableViewDataSource()
    {
        XCTAssertNotNil(libraryTableView.dataSource)
        XCTAssertTrue(libraryTableView.dataSource is NoteLibraryDataService)
    }
    
    // MARK: TableView Delegate
    
    func testDelegate_ViewDidLoad_SetsTableViewDelegate()
    {
        XCTAssertNotNil(libraryTableView.delegate)
        XCTAssertTrue(libraryTableView.delegate is NoteLibraryDataService)
    }
    
    // MARK: Data Source and Delegate as Same Object
    
    func testTableViewAttributes_IsSetToSameObject()
    {
        XCTAssertEqual(libraryTableView.dataSource as! NoteLibraryDataService, libraryTableView.delegate as! NoteLibraryDataService)
    }

}

