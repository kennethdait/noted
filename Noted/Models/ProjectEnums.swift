//
//  ProjectEnums.swift
//  Noted
//
//  Created by kend on 11/5/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation

// MARK:- ERROR ENUM (for NoteManager class)

enum NoteManagerErrorTypes: Error
{
    case UpdatingLibraryWithSameNote
}
