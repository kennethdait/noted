//
//  NoteManager.swift
//  Noted
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation

// MARK:- BASE CLASS: NoteManager
class NoteManager
{
    private var noteArray: [Note] = []
    var noteCount: Int { return noteArray.count }
    
    // MARK:- ADD & QUERY
    func addNote(_ note: Note, insertInto index: Int? = nil)
    {
        if let targetIndex = index
        {
            guard noteArray.indices.contains(targetIndex) else { fatalError() }
            noteArray.insert(note, at: targetIndex)
        }
        else
        { noteArray.append(note) }
        
    }
    
    func noteAt(index: Int) -> Note
    {
        guard noteArray.indices.contains(index) else { fatalError() }
        return noteArray[index]
    }
    
    // MARK:- UPDATE NOTES
    
    func updateNoteAt(index: Int, with updatedNote: Note) throws
    {
        let startingCount = noteArray.count
        guard noteArray.indices.contains(index) else { fatalError() }
        let oldNote = noteArray.remove(at: index)
        
        if oldNote == updatedNote
        { throw NoteManagerErrorTypes.UpdatingLibraryWithSameNote }
        
        guard noteArray.count == startingCount - 1 else { fatalError() }
        noteArray.insert(updatedNote, at: index)
        
        guard noteArray.count == startingCount else { fatalError() }
    }
    
    // MARK:- DELETE NOTE DATA
    
    func deleteNoteAt(index: Int) -> Note
    {
        guard noteArray.indices.contains(index) else { fatalError() }
        return noteArray.remove(at: index)
    }
    
    func clearAllNotes()
    {
        noteArray.removeAll()
    }
    
}
