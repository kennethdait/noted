//
//  Note.swift
//  Noted
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation

struct Note: Equatable
{
    // MARK:- PROPERTIES
    var content: String
    {
        didSet
        {
            self.dateLastModified = Date()
        }
    }
    
    private let dateCreated: Date
    private var dateLastModified: Date
    
    // MARK:- INITIALIZER
    init(content: String, dateCreated: Date? = nil)
    {
        self.content = content
        if let date = dateCreated
        { self.dateCreated = date }
        else
        { self.dateCreated = Date() }
        self.dateLastModified = self.dateCreated
    }
    
    // MARK:- METHODS
    func whenCreated() -> Date
    { return self.dateCreated }
    
    func whenLastModified() -> Date
    { return self.dateLastModified }
}
