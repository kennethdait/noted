//
//  DetailViewController.swift
//  Noted
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
    
    @IBOutlet weak var textView: UITextView!
    
    var masterVC: LibraryViewController!
    var dataToEdit: Note!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        textView.text = dataToEdit.content
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        masterVC.receivedNoteFromDetail = Note(content: textView.text, dateCreated: dataToEdit.whenCreated())
    }
    
    func configInitialData(note: Note)
    {
        
    }
    
}
