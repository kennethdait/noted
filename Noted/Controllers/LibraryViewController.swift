//
//  ViewController.swift
//  Noted
//
//  Created by kend on 10/13/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

// MARK:- BASE CLASS: LibaryViewController
class LibraryViewController: UIViewController
{

    let appTitle = "Noted"
    let appVersion = "v0.1-dev"
    
    @IBOutlet weak var libraryTableView: UITableView!
    @IBOutlet var dataService: NoteLibraryDataService!
    let noteManager = NoteManager()
    
    var newNoteButton: UIBarButtonItem!
    
    var selectedRowForDetail: Int = -1
    var receivedNoteFromDetail: Note? = nil
    var isAddingNewNote = false
    
    // MARK:- VIEW DID LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initializeDataHandling()
        setNavItemProperties()
        loadDummyData()
    }
    // MARK:-
    
    private func loadDummyData()
    {

        noteManager.addNote(Note(content: "test note 1"))
        noteManager.addNote(Note(content: "test note 2"))
        noteManager.addNote(Note(content: "test note 3"))
        noteManager.addNote(Note(content: "test note 4"))
        noteManager.addNote(Note(content: "test note 5"))
        noteManager.addNote(Note(content: "test note 6"))
        
        var testNote1 = Note(content: "test note", dateCreated: Date().addingTimeInterval(-1000000))
    }
    
}



//
// MARK:- CONFIGURE: DATA HANDLING
//

extension LibraryViewController
{
    
    private func initializeDataHandling()
    {
        dataService.noteManager = noteManager
        dataService.masterVC = self
        libraryTableView.dataSource = dataService
        libraryTableView.delegate = dataService
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
    }
    
    @objc func createNewNote()
    {
        let newNote = Note(content: "new note \(noteManager.noteCount + 1)")
        selectedRowForDetail =  dataService.createNewNoteRow(tableView: libraryTableView, note: newNote)
        readyToExecuteShowDetailSegue(isAddingNewNote: true)
    }
}



//
// MARK:- CONFIGURE: NAV ITEM PROPERTIES
//

extension LibraryViewController
{
    private func setNavItemProperties()
    {
        title = appTitle
        newNoteButton = UIBarButtonItem(barButtonSystemItem: .add,
                                        target: self,
                                        action: #selector(createNewNote)
        )
        
        navigationItem.leftBarButtonItem = editButtonItem
        navigationItem.rightBarButtonItem = newNoteButton
    }
}



//
// MARK:- SET EDITING
//

extension LibraryViewController
{
    // NOTE: tableView(_:commit:forRowAt:) should be set in dataService
    
    override func setEditing(_ editing: Bool, animated: Bool)
    {
        super.setEditing(editing, animated: true)
        libraryTableView.setEditing(editing, animated: true)
    }
    
    
}


//
// MARK:- RESPOND TO SEGUES
//

extension LibraryViewController
{
    
    func readyToExecuteShowDetailSegue(isAddingNewNote: Bool? = false)
    {
        
        
        
        if isAddingNewNote == true
        { self.isAddingNewNote = true }
        
        performSegue(withIdentifier: "showDetail", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?)
    {
        if segue.identifier == "showDetail"
        {
            let targetNote = dataService.getNoteFromTableRow(tableView: libraryTableView, index: selectedRowForDetail)
            
            let detailVC = segue.destination as! DetailViewController
            detailVC.configInitialData(note: targetNote)
            detailVC.masterVC = self
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard selectedRowForDetail != -1 else { return }
        guard let updatedNote = receivedNoteFromDetail else { return }
        
        dataService.updateNote(tableView: libraryTableView, updateNote: updatedNote, index: selectedRowForDetail)
    }
    
}
