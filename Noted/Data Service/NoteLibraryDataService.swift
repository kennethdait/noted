//
//  NoteLibraryDataService.swift
//  Noted
//
//  Created by kend on 11/4/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

class NoteLibraryDataService: NSObject {
    var noteManager: NoteManager?
    var masterVC: LibraryViewController?
}

// MARK:- TABLE CONFIG

extension NoteLibraryDataService: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    { return 1 }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int
    {
        guard let noteManager = noteManager else { fatalError() }
        return noteManager.noteCount
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let noteManager = noteManager else { fatalError() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCellID", for: indexPath)
        let targetNote = noteManager.noteAt(index: indexPath.row)
        cell.textLabel?.text = targetNote.content
        cell.detailTextLabel?.text = String(describing: targetNote.whenCreated())
        return cell
    }
    
}

// MARK:- ADD/DELETE ROWS

extension NoteLibraryDataService
{
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath)
    {
        // method for table deletion
        guard let noteManager = noteManager else { fatalError() }
        
        
        // 1. delete represented data in noteManager
        _ = noteManager.deleteNoteAt(index: indexPath.row)
        
        // 2. delete table row
        //tableView.deselectRow(at: indexPath, animated: true)
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadData()
    }
    
    func createNewNoteRow(tableView: UITableView, note: Note) -> Int
    {
        guard let noteManager = noteManager else { fatalError() }
        
        noteManager.addNote(note, insertInto: 0)
        
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        
        guard let selectedRow = tableView.indexPathForSelectedRow?.row else { fatalError() }
        tableView.deselectRow(at: IndexPath(row: selectedRow, section: 0), animated: false)
        return selectedRow
    }
    
    func getNoteFromTableRow(tableView: UITableView, index: Int) -> Note
    {
        guard let noteManager = noteManager else { fatalError() }
        return noteManager.noteAt(index: index)
    }
    
    func insertNewNote(tableView: UITableView, note: Note, index: Int)
    {
        guard let noteManager = noteManager else { fatalError() }
        noteManager.addNote(note, insertInto: index)
        tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    func updateNote(tableView: UITableView, updateNote: Note, index: Int)
    {
        guard let noteManager = noteManager else { fatalError() }
        
        
        try? noteManager.updateNoteAt(index: index, with: updateNote)
        let targetIndexPath = IndexPath(row: index, section: 0)
        tableView.selectRow(at: targetIndexPath, animated: true, scrollPosition: .none)
        tableView.deselectRow(at: targetIndexPath, animated: false)
    }
    
}

// MARK:- DELEGATE METHODS

extension NoteLibraryDataService: UITableViewDelegate
{
    
    // MARK: MANAGING SELECTIONS
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard let selectedRow = tableView.indexPathForSelectedRow?.row else { fatalError() }
        guard let masterVC = masterVC else { fatalError() }
        masterVC.selectedRowForDetail = selectedRow
        tableView.deselectRow(at: indexPath, animated: false)
        masterVC.readyToExecuteShowDetailSegue()
    }
}
